package anagramprogram;

import java.util.Arrays;

public class anagram {
	
	public static void main(String[] args) {
		
		System.out.println("anagrams are "+checkAnagram("break","baker"));
	}
	
	
	static boolean checkAnagram(String str1,String str2)
	{
	
		char[] strArray1=str1.toUpperCase().toCharArray();
		char[] strArray2=str2.toUpperCase().toCharArray();
		Arrays.sort(strArray1);
		Arrays.sort(strArray2);
		if(Arrays.equals(strArray1, strArray2))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}
